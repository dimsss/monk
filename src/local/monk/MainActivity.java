package local.monk;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Calendar;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import local.monklogic.*;

public class MainActivity extends Activity {

	private DBHelper db;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		db = getDb();
		updateLastEventLable();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public DBHelper getDb() {
		return new DBHelper(this);
	}
	public void bnt1Click(View view) throws InterruptedException{
		insertEvent("1");
	}
	public void bnt2Click(View view){
		insertEvent("2");
	}
	public void btnLogViewClick(View view){
		viewEvents();
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.exportCSV:
			try {
				exportCSV();
				db.deleteEvents();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		case R.id.exit:
			finish();
			return true;
		case R.id.exportToEmail:
			messageBox("This functionality is not implemented in this version");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void insertEvent(String eventSource){
		ContentValues cv = new ContentValues();
		Long time = System.currentTimeMillis();
		cv.put("time", time);
		cv.put("eventSource", eventSource);
		db.insertEvent(cv);
		updateLastEventLable();

	}

	public void viewEvents(){
		AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
		dlgAlert.setPositiveButton("OK", null);
		dlgAlert.setCancelable(true);
		dlgAlert.setTitle("The latest events");
		dlgAlert.setMessage(buildUserOutput(db.selectLatestEvents()," "));
		dlgAlert.create().show();
	}

	public String buildUserOutput(Cursor cursor, String splitCh){

		String message = "";
		if (cursor.moveToFirst()){
			do {
				String userOutput = parsToUserString(cursor.getLong(cursor.getColumnIndex("time")),splitCh);
				userOutput += splitCh + cursor.getString(cursor.getColumnIndex("eventSource"));
				message += userOutput+"\n";
			}while (cursor.moveToNext());
		}
		return message;
	}

	public void updateLastEventLable(){
		TextView eventLab= (TextView) findViewById(R.id.lastLogEventLable);
		Cursor cursor = db.getLastEvent();
		if(cursor.moveToFirst()){
			String userOutput = parsToUserString(cursor.getLong(cursor.getColumnIndex("time"))," ");
			userOutput += " " + cursor.getString(cursor.getColumnIndex("eventSource"));
			eventLab.setText(userOutput);
		}else
			eventLab.setText("No records found in DB");

	}

	public String parsToUserString(long timestamp,String splitCh){
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timestamp);
		String userOutputStr = Integer.toString(c.get(Calendar.DAY_OF_MONTH))+"/"+
				Integer.toString(c.get(Calendar.MONTH))+"/"+
				Integer.toString(c.get(Calendar.YEAR));
		userOutputStr += splitCh;
		userOutputStr += Integer.toString(c.get(c.HOUR_OF_DAY));
		if ( c.get(c.MINUTE) <10 )
			userOutputStr +=":0";
		else 
			userOutputStr += ":";
		userOutputStr += Integer.toString(c.get(Calendar.MINUTE));

		if (c.get(c.SECOND) < 10 )
			userOutputStr += ":0";
		else 
			userOutputStr += ":";
		userOutputStr += Integer.toString(c.get(Calendar.SECOND));

		return userOutputStr;
	}

	public void messageBox(String message){
		Toast.makeText(getApplicationContext(), 
				message, Toast.LENGTH_LONG).show();
	}


	public int exportCSV() throws IOException{
		if (!isExternalStorageWritable()){
			messageBox("Can't write to external storage");
			return 1;
		}
		File workDir = getStorageDir("Monk_export");
		String date = parsToUserString(System.currentTimeMillis(), " ").replace("/", "-");
		String fileName = "export_"+date+".txt";
		File csvExportFile = new File(workDir,fileName);
		FileOutputStream fos = new FileOutputStream(csvExportFile);
		OutputStreamWriter osw = new OutputStreamWriter(fos);
		osw.write(buildUserOutput(db.selectAllEvents(), ","));
		osw.flush();
		osw.close();
		messageBox("File saved at: "+csvExportFile.getAbsolutePath());
		return 0;
	}


	public boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}

	public File getStorageDir(String dirName) { 
		File file = new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_DOWNLOADS), dirName);
		file.mkdirs();
		return file;
	}
}