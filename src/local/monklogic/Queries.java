package local.monklogic;

public class Queries {
	
	static final String CREATE_EVENT_LOG_TBL = "CREATE TABLE event_log ("+		
											   "time           INTEGER      NOT NULL,"+
											   "eventSource    TEXT     	NOT NULL );";
	
	static final String DROP_EVENT_LOG_TBL 	= "DROP TABLE IF EXISTS event_log;";
	
	static final String DELETE_EVENTS 	    = "DELETE FROM event_log WHERE time < (SELECT max(time) FROM event_log);";
	
	static final String GET_LAST_RECORD 	= "SELECT * FROM event_log ORDER BY time DESC LIMIT 1;";
	
	static final String GET_LATEST_EVENTS 	= "SELECT * FROM event_log ORDER BY time DESC LIMIT 10;";
	
	static final String GET_ALL_EVENTS	 	= "SELECT * FROM event_log ORDER BY time DESC;";

}
