package local.monklogic;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

	static final String DB_NAME = "monkDB";
	static final String EVENT_LOG_TBL = "event_log";
	static final int DB_VERSION = 9;
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		//System.out.println("DBHELPER CONSTRUCTOR!!!!");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(Queries.CREATE_EVENT_LOG_TBL);
		//System.out.println("DB "+DB_NAME+" IS CREATED");

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(Queries.DROP_EVENT_LOG_TBL);
		//System.out.println("DB "+DB_NAME+" IS DROPPED");
		onCreate(db);
	}

	public void deleteEvents(){
		this.getWritableDatabase().execSQL(Queries.DELETE_EVENTS);
		//System.out.println("DB "+DB_NAME+" IS DELETED");
	}

	public void insertEvent(ContentValues cv){
		this.getWritableDatabase().insert(EVENT_LOG_TBL, null, cv);
		//System.out.println("New row added to "+DB_NAME);
	}

	public Cursor selectLatestEvents(){
		return this.getWritableDatabase().
				rawQuery(Queries.GET_LATEST_EVENTS, null);
	}
	
	public Cursor selectAllEvents(){
		return this.getWritableDatabase().
				rawQuery(Queries.GET_ALL_EVENTS, null);
	}

	public int getDBVersion(){
		return this.getWritableDatabase().getVersion();
	}

	public Cursor getLastEvent(){
		return this.getWritableDatabase().
				rawQuery(Queries.GET_LAST_RECORD, null);
	}

}
